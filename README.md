# Discourse theme component for the ISSUP forum homepage banner.

Initially developed using the branding plugin, this component is now
self-contained. Install from the web by pasting the repository URL into the
Import box on /admin/customize/themes
